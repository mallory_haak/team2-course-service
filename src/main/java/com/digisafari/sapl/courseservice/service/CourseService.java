package com.digisafari.sapl.courseservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digisafari.sapl.courseservice.exceptions.CourseAlreadyExistsException;
import com.digisafari.sapl.courseservice.exceptions.CourseNotFoundException;
import com.digisafari.sapl.courseservice.model.Course;
import com.digisafari.sapl.courseservice.repository.CourseRepository;

@Service
public class CourseService implements ICourseService {
	
	@Autowired
	CourseRepository courseRepository;

	public Course addCourse(Course course) throws CourseAlreadyExistsException {
		/*
		 * 1. check whether the course is existing in the database or not 
		 * 2. if it is existing then throw CourseAlreadyExistsException
		 * 3. else add the course to the database and return the added course as response back to controller
		 */
		Course createdCourse = null;
		try {
			Optional<Course> optionalCourse = courseRepository.findById(course.getId());
			if(optionalCourse.isPresent()) {
				throw new CourseAlreadyExistsException();
			} else {
				createdCourse = courseRepository.save(course);
			}
		} catch (CourseAlreadyExistsException e) {
			throw e;
		}
		return createdCourse;
	}

	public List<Course> getAllCourses() {
		// TODO Auto-generated method stub
		return courseRepository.findAll();
	}

	public Course getCourseById(String id) throws CourseNotFoundException {
		// TODO Auto-generated method stub
		Course courseById = null;
		try {
			Optional<Course> optionalCourse = courseRepository.findById(id);
			if(optionalCourse.isPresent()) {
				courseById = optionalCourse.get();
			} else {
				throw new CourseNotFoundException();
			}
		} catch (CourseNotFoundException e) {
			throw e;
		}
		return courseById;
	}

	public Course updateCourse(Course course) throws CourseNotFoundException {
		// TODO Auto-generated method stub
		Course updatedCourse = null;
		try {
			Optional<Course> optionalCourse = courseRepository.findById(course.getId());
			if(optionalCourse.isPresent()) {
				updatedCourse = courseRepository.save(course);
			} else {
				throw new CourseNotFoundException();
			}
		} catch (CourseNotFoundException e) {
			throw e;
		}
		return updatedCourse;
	}

	public boolean deleteCourse(String id) throws CourseNotFoundException {
		// TODO Auto-generated method stub
		boolean isDeleted = false;
		try {
			Optional<Course> optionalCourse = courseRepository.findById(id);
			if(optionalCourse.isPresent()) {
				courseRepository.delete(optionalCourse.get());
				isDeleted = true;
			} else {
				throw new CourseNotFoundException();
			}
		} catch (CourseNotFoundException e) {
			throw e;
		}
		return isDeleted;
	}

}
