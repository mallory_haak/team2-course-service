package com.digisafari.sapl.courseservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Course does not exist with the ID")
public class CourseNotFoundException extends Exception {

}
